class BooksController < ApplicationController
  def show
		@books = Book.all
  end


	def get_prices
		@books = Book.all
		@prices = @books.map(&:price)
		respond_to do |format|
			format.json { render json: { prices_of_the_books: @prices } } 
			format.html { redirect_to books_show_path }
		end
	end
end
