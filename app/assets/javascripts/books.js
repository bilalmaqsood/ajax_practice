$(document).ready(function() {
	$("#getPricesButton").click(function() {
		$.ajax({
			type: 'GET',
		  url: 'http://localhost:3000/books/get_prices/',
		  dataType: 'JSON',
		  success: function(data) {
				var i = 0;
				for(; i < data.prices_of_the_books.length; i++) {
					$("#bookPrice" + (i+1)).html("Price: " + data.prices_of_the_books[i])
				}
			}
		});
	});
});
